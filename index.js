//Arrow Syntax
const golden = () => {
	console.log ("This is golden!")};

golden();

//Object Literal
const newFunction = function  literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

//Destructuring
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

let {firstName, lastName, destination, occupation}= newObject;

console.log(firstName, lastName, destination, occupation);

//Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)


//Template Literal
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam `
// Driver Code 
console.log(before) 